.headers on
.mode csv
.output data.csv

SELECT
  a.avg_tuition,
  a.total_students,
  a.name AS campus_name,
  a.international_students
FROM
  core_schoolcampus a;
