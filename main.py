#!/bin/env python
import random
import pandas as pd
import numpy as np
import argparse
from matplotlib import pyplot as plt

def min_max_scaling(series:pd.Series) -> pd.Series:
    """
    Min-Max Scaling

    Performs Min-Max scaling for the given series
    """
    min_value = series.min()
    range_value = series.max() - min_value
    return series.map(lambda x: (x - min_value)/(range_value))


def choose_random_points(series:pd.DataFrame, n:int) -> list:
    """
    Returns a list of tuples with N random points chosen form the series
    """
    tuple_records = list(series.itertuples(index=False, name=None))
    if n > len(tuple_records):
        raise ValueError("N is larger than the available choices")

    choices = []
    while len(choices) < n:
        choice = random.choice(tuple_records)
        if choice not in choices:
            choices.append(choice)

    return choices


def get_euclidean_distance(a: tuple, b: tuple) -> float:
    """
    Given two points finds the Euclidean distance between them
    """
    dimensions = 0
    start = 0
    if len(a) < len(b):
        dimensions = len(a)
        start = len(b) - dimensions
        b = b[start:]
    else:
        dimensions = len(b)
        start = len(a) - dimensions
        a = a[start:]

    distance_sum = 0
    for i in range(start, dimensions):
        distance_sum += (a[i] - b[i]) ** 2
    return distance_sum ** (1/2)


def get_mean_centroid(points:list, dimensions: int) -> tuple:
    """
    Given a set of points, finds the location of the mean point
    """
    if len(points) > 0:
        start = 0
        if dimensions < len(points[0]):
            start = len(points[0]) - dimensions
        mean_centroid = []
        for i in range(start, dimensions + 1):
            mean_centroid.append(
                np.mean([point[i] for point in points])
            )
        return tuple(mean_centroid)
    return tuple([np.Inf] * dimensions)


def get_inertia(centroid:tuple, points:list) -> float:
    """
    Given a set of points, determines what's its inertia
    """
    return sum([get_euclidean_distance(centroid, x) ** 2 for x in points])


def generate_clusters(data: pd.DataFrame, centroids: list) -> list:
    """
    Given the treated data and centroids, it generates the clusters
    """
    clusters = [[] for _ in range(len(centroids))]
    inertias = []
    for row in data.itertuples(name=None):
        distances = []
        for centroid in centroids:
            distances.append(get_euclidean_distance(centroid, row))
        cluster_id = np.argmin(distances)
        clusters[cluster_id].append(row)

    for idx, centroid in enumerate(centroids):
        inertias.append(get_inertia(centroid, clusters[idx]))

    return clusters, inertias


def clusters_to_dataframe(clusters: list, orig: pd.DataFrame) -> pd.DataFrame:
    """
    Given the clusters, it appends the cluster id to each datapoint
    and returns the tuple as a DataFrame
    """
    merged_data = []
    columns = ["id"]
    columns += list(orig.columns)
    columns.append("cluster")
    for i in range(len(clusters)):
        for datapoint in clusters[i]:
            merged_data.append((*datapoint, i))
    df = pd.DataFrame(merged_data, columns=columns)
    return df.set_index("id")


def kmeans(data:pd.DataFrame, n:int=3, max_epochs:int=100):
    """
    K-Means Clustering Method

    Notes
    -----
    * It only takes into account numeric columns for the clustering. Bear in
      mind that Pandas is unable to distinguish if a numerical column is
      actually a categorical column.
    """
    filtered_columns = []
    for col in list(data.columns):
        if data[col].dtype != "object":
            # Scale this column
            data[col] = min_max_scaling(data[col])
            # Add this column to the filtered list
            filtered_columns.append(col)
    treated_data = data[filtered_columns]
    centroids = choose_random_points(treated_data, n)
    clusters, inertias = generate_clusters(treated_data, centroids)
    for i in range(max_epochs):
        prev_centroids = centroids
        centroids = [
            get_mean_centroid(cluster, len(filtered_columns))\
            for cluster in clusters
        ]
        clusters, inertias = generate_clusters(treated_data, centroids)
        change = np.mean([
            get_euclidean_distance(prev, curr)\
            for prev, curr in zip(prev_centroids, centroids)
        ])
        if change == 0: # Stop the for if the centroid is not changing
            break
    return clusters_to_dataframe(clusters, treated_data), np.mean(inertias)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Applies k-means clustering to a given dataset"
    )
    parser.add_argument("data", type=str)
    args = parser.parse_args()
    df = pd.read_csv(args.data)
    results = []
    elbow_data = []
    for i in range(2,10+1):
        _results = []
        _inertias = []
        for j in range(10):
            result, inertia = kmeans(df, i)
            _results.append(result)
            _inertias.append(inertia)
        result = _results[np.argmin(_inertias)]
        inertia = np.min(_inertias)
        print(f"N = {i}, Inertia = {inertia}")
        elbow_data.append((i, inertia))
        results.append(result)


    x, y = zip(*elbow_data)
    plt.xlabel("Number of Clusters")
    plt.ylabel("Inertia")
    plt.plot(x, y)
    plt.grid()
    plt.tight_layout()
    plt.show()

    num_clusters = int(input("How many clusters do you want? "))
    if num_clusters <= 10:
        cluster_id = num_clusters - 2

    cluster_tags = results[cluster_id]["cluster"].unique()
    ax = plt.axes(projection="3d")
    ax.set_xlabel(results[0].columns[0])
    ax.set_ylabel(results[0].columns[1])
    ax.set_zlabel(results[0].columns[2])

    for cluster_tag in cluster_tags:
        cluster = list(results[cluster_id][
            results[cluster_id]["cluster"] == cluster_tag
        ].itertuples(index=False, name=None))
        ax.scatter3D(
            [point[0] for point in cluster],
            [point[1] for point in cluster],
            [point[2] for point in cluster]
        )

    plt.show()

    results[cluster_id].to_csv("result.csv")

